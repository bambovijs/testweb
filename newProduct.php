<?php 
include('config.php');
include('head.php');
include('navigation.php');
include('product.php');
?>
<div class="container">
    <h1>Add new Product</h1>
    <p>Please fill this form!</p>
</div>
<div class="container" style="margin-top: 20px;">  
            <?php 
            print "<form action=\"addNewProduct.php\" method=\"post\">";
                print "<div class=\"form-group\">";
                    print "<div class=\"col-4\">";

                        //Izveidojam SQL vaicajumu nakamajam produkta ID, jo ieks mysql liku Auto Increment
                        $sql_vaicajums="SELECT * FROM product;";

                        $result=mysqli_query($conn,$sql_vaicajums);

                        $nextPID = mysqli_num_rows($result);
                        

                        print "<label for=\"pID\">Product ID</label>";
                        //ar disable nestrada, jo nepanjem ID
                        print "<input type=\"text\" class=\"form-control\" name=\"pID\" value=\"$nextPID\">";
                        print "<br>";

                        print "<label for=\"name\">Product Name</label>";
                        print "<input type=\"text\" class=\"form-control\" name=\"name\" placeholder=\"Product Name\">";
                        print "<br>";

                        print "<label for=\"price\">Product Price</label>";
                        print "<input type=\"text\" class=\"form-control\" name=\"price\" placeholder=\"Product Price\">";
                        print "<br>";

                        print "<label for=\"productType\">Product Type</label>";
                        print "<select id=\"category_cID\" name=\"category_cID\" class=\"custom-select mr-ms-2\">";
                        print "<option value=\"0\" selected>Choose type</option>";
                        print "<option value=\"1\">DVD-disc</option>";
                        print "<option value=\"2\">Book</option>";
                        print "<option value=\"3\">Furniture</option>";
                        print "</select><br><br>";

                        print "<div id=\"typeFieldDisc\">";
                            print "<label for=\"discSize\">Disc Size</label>";
                            print "<input type=\"text\" class=\"form-control\" id=\"discSize\" placeholder=\"Disc Size\">";
                            print "<small id=\"discSize\" class=\"form-text text-muted\">Disc size in MB</small>";
                        print "</div>";

                        print "<div id=\"typeFieldBook\">";
                            print "<label for=\"bookWeight\">Book Weight</label>";
                            print "<input type=\"text\" class=\"form-control\" id=\"bookWeight\" placeholder=\"Book Weight\">";
                            print "<small id=\"bookWeight\" class=\"form-text text-muted\">Book weight in KG</small>";
                        print "</div>";

                        print "<div id=\"typeFieldFurniture\">";
                            print "<label for=\"furnitureDimentions\">Furniture Dimentions</label>";
                            print "<input type=\"text\" class=\"form-control\" id=\"furnitureDimentionsH\" placeholder=\"Height\"><br>";
                            print "<input type=\"text\" class=\"form-control\" id=\"furnitureDimentionsW\" placeholder=\"Width\"><br>";
                            print "<input type=\"text\" class=\"form-control\" id=\"furnitureDimentionsL\" placeholder=\"Length\">";
                            print "<small id=\"furnitureDimentions\" class=\"form-text text-muted\">Furniture dimentions (Height x Width x Length)</small>";
                        print "</div><br>";
                    
                        print "<button type=\"submit\" class=\"btn btn-primary mb-2\">Submit</button>";

                    print "</div>";
                print "</div>";
            print "</form>";
            ?>




</div>
            
</div>
            
<script>
    //paslepjam typeField
    $('#typeFieldDisc').hide();
    $('#typeFieldBook').hide();
    $('#typeFieldFurniture').hide();
    //ja selecto ar id 1 tad DVD typeField parādās
    $('#category_cID').change(function(){
        if($(this).val() == "1"){
            $("#typeFieldBook").hide();
            $("#typeFieldFurniture").hide();
            $("#typeFieldDisc").show();
        }
        else if($(this).val() == "2" ){
            $("#typeFieldDisc").hide();
            $("#typeFieldFurniture").hide();
            $("#typeFieldBook").show();
        }
        else if($(this).val() == "3"){
            $("#typeFieldDisc").hide();
            $("#typeFieldBook").hide();
            $("#typeFieldFurniture").show();
        }
        else{
            $('#typeFieldDisc').hide();
            $('#typeFieldBook').hide();
            $('#typeFieldFurniture').hide();
        }
    });
</script>
<?php   
//$obj = new Product(1, "nosaukums", 100);
    
   // echo $obj->productName;
?>

<?php include('bottom.php');

