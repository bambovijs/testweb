<?php 
include('config.php');
include('head.php');
include('navigation.php');
?>

<!-- html kods pa vidu --> 
<div class="container">
  <h1>Product List</h1>
    <!-- Izveido objektu un tad attēlo, jāliek ciklā kopā ar card -->
     <?php
        $sql = "SELECT * FROM product";
        $result = mysqli_query($conn, $sql);
        if ($result->num_rows > 0) {
         // output data of each row
            print '<div class="row">';
            while($row = $result->fetch_assoc()) {
                
                print '<div class="col-sm-3">';
                    print '<div class="card bg-light">';
                        print '<div class="card-body text-center">';
                            print '<div class="checkbox" style="margin: 10px 0px 0px 20px;">';
                                print '<label><input type="checkbox" value=""></label>';
                
                                print '<p class="card-text">' .$row["pID"]. '</p>';
                                print '<p class="card-text">' .$row["name"]. '</p>';
                                print '<p class="card-text">' .$row["price"]. '</p>';
                                print '<p class="card-text">' .$row["category_cID"]. '</p>';
                            print '</div>';
                        print '</div>';
                    print '</div><br>';
                print '</div>';
                
            }
            print '</div>';
        }
    ?>
</div>

<?php
include('bottom.php');
?>
